# Jass-Punktetafel

Jass-Punktetafel - Der Punktezähler beim Jassen
Bei diversen Jassvarianten ist das Punktezählen aufwendig, da hat es der Schreiber schwer. Daher gibt es dafür eine einfache App welche die Punkte für beide Teams zusammenzählt.

## Installation

Du kannst die App vom F-Droid Store installieren


## Screenshots
<p align="left">
  <img alt="Light" src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="30%">
    &nbsp; &nbsp; &nbsp; &nbsp;
  <img alt="Dark" src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="30%">
</p>

## License
~~~
MIT License

Copyright (c) 2021

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
~~~
