﻿using Microsoft.AspNetCore.Components.Web;
using System;
using System.Timers;

namespace Jassen
{
    public class KeyboardHelper
    {
        private static readonly Timer _hideKeyboardTimer = new Timer(3000);

        static KeyboardHelper()
        {
            _hideKeyboardTimer.Elapsed += HideKeyboardTimer_Elapsed;
        }

        public static void ResetHideKeyboardTimer()
        {
            _hideKeyboardTimer.Stop();
            _hideKeyboardTimer.Start();
        }

        private static void HideKeyboardTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            HideKeyboard();
        }

        public static void HideKeyboardOnEnter(KeyboardEventArgs e)
        {
            if (e.Code == "Enter" || e.Code == "NumpadEnter" || e.Key == "Enter" || e.Key == "NumpadEnter")
            {
                HideKeyboard();
            }
        }

        public static void HideKeyboard()
        {
            _hideKeyboardTimer.Stop();
            HideKeyboardEvent?.Invoke(null, new EventArgs());
        }

        public static event EventHandler HideKeyboardEvent;
    }
}