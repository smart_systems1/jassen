﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Jassen
{
    public class JassInfos
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Teamname1 { get; set; }
        public string Teamname2 { get; set; }
        public IEnumerable<JassResult> Results { get; set; }
        public int? ResultTeam1 { get; set; }
        public int? ResultTeam2 { get; set; }

        public JassInfos(string title, string description, IEnumerable<JassResult> jassResults)
        {
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Description = description ?? throw new ArgumentNullException(nameof(description));
            Results = jassResults ?? throw new ArgumentNullException(nameof(jassResults));
        }

        public void CalculateResult()
        {
            var result1 = 0;
            var result2 = 0;
            foreach (var result in Results)
            {
                result1 += result.Factor * result.Result1 ?? 0;
                result2 += result.Factor * result.Result2 ?? 0;
            }
            if (result1 == 0) { ResultTeam1 = null; } else { ResultTeam1 = result1; }
            if (result2 == 0) { ResultTeam2 = null; } else { ResultTeam2 = result2; }
        }

        public bool IsResultOK()
        {
            return Results.All(x => x.IsResultOK());
        }
    }

    public class JassResult
    {
        public string Name { get; set; }
        public int Factor { get; set; }

        private int? _result1;
        public int? Result1 { get => _result1; set { _result1 = value; KeyboardHelper.ResetHideKeyboardTimer(); } }

        private int? _result2;
        public int? Result2 { get => _result2; set { _result2 = value; KeyboardHelper.ResetHideKeyboardTimer(); } }

        public JassResult(string name, int factor = 1)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Factor = factor;
        }

        public bool IsResultSet(int teamNumber = 0)  // 0 = all teams
        {
            if ((teamNumber == 0 || teamNumber == 1) && (Result1 == null) ||
               (teamNumber == 0 || teamNumber == 2) && (Result2 == null))
                return false;
            return true;
        }

        public string GetStyle(int teamNumber)
        {
            if (!IsResultSet(teamNumber)) return "";
            else if (!IsResultOK(teamNumber)) return "color:red";
            else return "background:lightgreen";
        }

        public bool IsResultOK(int teamNumber = 0)  // 0 = all teams
        {
            if ((teamNumber == 0 || teamNumber == 1) && Result1 != null && (Result1 < 0 || (Result1 > 157 && Result1 != 257)) ||
               (teamNumber == 0 || teamNumber == 2) && Result2 != null && (Result2 < 0 || (Result2 > 157 && Result2 != 257)))
                return false;
            return true;
        }
    }
}